#!/bin/bash

nameProp=$(grep "level-name=.*" server.properties)

if [[ $? -ne 1 ]]; then
	echo "Couldn't get map name from server.properties file"
	exit 1
fi

mapName=$(echo $nameProp | sed "s/[^=]*=//")
backupDir=backups
copy=$backupDir/$mapName
latest=latest.tar.gz
shortDate=`date +\%y.\%m.\%d.\%H`
tarfile=$backupDir/$mapName.$shortDate.tar.gz
sizeThreshold=$[1024 * 16] # byte difference in file size before a new map backup is made
timeThreshold=$[60*60*24*7*2] # the longest time to go between backups in seconds
override=$1

echo `date`;

cd /data/minecraft;

# Create necessary directories if they don't exist
[ -d $backupDir ] || mkdir -p $backupDir;
[ -d $backupDir/leveldat ] || mkdir $backupDir/leveldat;

# Always make a backup of level.dat (player inventories, etc)
cp $mapName/level.dat $backupDir/leveldat/$mapName.$shortDate.level.dat && echo "Backup of level.dat successful";

# Check the date of the latest backup - always back up once every two weeks
timeDiff=$[ `date +%s` - `stat -c %Z $copy` ]

# Check the file size difference in bytes
sizeDiff=$[ `du -s $mapName | cut -f 1` - `du -s $copy | cut -f 1` ]
[ ! $override ] && [ $timeDiff -lt $timeThreshold ] && [ $sizeDiff -lt $sizeThreshold ] && echo "Thresholds not met, no map backup made ($timeDiff seconds, $sizeDiff bytes)" && exit;

./sendCommand "say temporarily disabling world saving";
./sendCommand "save-all";
./sendCommand "save-off";
[ -d $copy ] && rm -r $copy;
cp -r $mapName $copy;
./sendCommand "save-on";
./sendCommand "say backup complete, world saving re-enabled";

tar -czf $tarfile $copy;
ln -f $tarfile $latest;

